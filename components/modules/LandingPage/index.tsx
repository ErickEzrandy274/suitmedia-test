import { Box, Flex, Heading } from "@chakra-ui/react";
import { ContactUsForm, LandingPageCarousel, ValuesCard } from "@elements";
import React from "react";
import { ourValuesData } from "./constant";

const LandingPage = () => {
	return (
		<Flex flexDir="column" gap={10}>
			<LandingPageCarousel />

			<Flex flexDir="column" textAlign="center" gap={5} p={5}>
				<Heading>Our Values</Heading>
				<Flex
					flexDir={{ base: "column", md: "row" }}
					justifyContent="center"
					gap={10}
					color="white"
				>
					{ourValuesData.map((item, index) => {
						return (
							<ValuesCard
								key={item.type}
								{...item}
								isLast={index + 1 === ourValuesData.length}
							/>
						);
					})}
				</Flex>
			</Flex>

			<Flex flexDir="column" textAlign="center" gap={5} p={5}>
				<Heading>Contact Us</Heading>
				<ContactUsForm />
			</Flex>
		</Flex>
	);
};

export default LandingPage;
