const color = {
	innovative: "#F56565",
	loyalty: "#48BB78",
	respect: "#4299E1",
};

const borderColor = {
	innovative: "#E53E3E",
	loyalty: "#38A169",
	respect: "#3182CE",
};

export const getColor = (key: "innovative" | "loyalty" | "respect") => {
	return color[key];
};

export const getBorderColor = (key: "innovative" | "loyalty" | "respect") => {
	return borderColor[key];
};
