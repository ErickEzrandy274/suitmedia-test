import { LandingPage } from "@modules";
import React from "react";

const Home = () => {
	return <LandingPage />;
};

export default Home;
