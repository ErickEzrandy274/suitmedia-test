import React from "react";
import { ValuesCardProps } from "./interface";
import { Box, Flex, Heading, Text } from "@chakra-ui/react";
import { getBorderColor, getColor } from "@utils";

const ValuesCard: React.FC<ValuesCardProps> = ({
	icon,
	title,
	description,
	type,
	isLast,
}) => {
	return (
		<Flex
			flexDir="column"
			gap={3}
			bg={getColor(type)}
			border={`3px solid ${getBorderColor(type)}`}
			p={5}
			w={{ md: "25%" }}
			position="relative"
		>
			<Flex justifyContent="center">{icon}</Flex>
			<Heading textTransform="uppercase" fontSize="xl">
				{title}
			</Heading>
			<Text fontWeight="medium" fontSize="sm">
				{description}
			</Text>

			{!isLast && (
				<Box
					w={10}
					h={10}
					display={{ base: "none", md: "block" }}
					bg={getColor(type)}
					transform="rotate(45deg)"
					position="absolute"
					top="40%"
					right={-5}
				/>
			)}
		</Flex>
	);
};

export default ValuesCard;
