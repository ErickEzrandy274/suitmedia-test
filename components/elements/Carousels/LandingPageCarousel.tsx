import React from "react";
import { Box, Container, IconButton, Stack, Text } from "@chakra-ui/react";
import { ChevronLeftIcon, ChevronRightIcon } from "@chakra-ui/icons";
import { cards, settings } from "./constant";
import Slider from "react-slick";

export default function Carousel() {
	const [slider, setSlider] = React.useState<Slider | null>(null);

	return (
		<Box
			position="relative"
			height={{ base: "full", md: "fit-content" }}
			width="full"
			overflow="hidden"
		>
			{/* CSS files for react-slick */}
			<link
				rel="stylesheet"
				type="text/css"
				charSet="UTF-8"
				href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
			/>
			<link
				rel="stylesheet"
				type="text/css"
				href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
			/>
			{/* Left Icon */}
			<IconButton
				variant="outline"
				aria-label="left-arrow"
				rounded="full"
				position="absolute"
				border="2px solid white"
				left="10px"
				top="50%"
				transform={"translate(0%, -50%)"}
				zIndex={2}
				onClick={() => slider?.slickPrev()}
				colorScheme="white"
				_hover={{ bg: "white" }}
				icon={
					<ChevronLeftIcon
						color="white"
						w="inherit"
						h="inherit"
						_hover={{ color: "black" }}
					/>
				}
			/>
			{/* Right Icon */}
			<IconButton
				variant="outline"
				aria-label="right-arrow"
				rounded="full"
				border="2px solid white"
				position="absolute"
				right="10px"
				top="50%"
				transform={"translate(0%, -50%)"}
				zIndex={2}
				onClick={() => slider?.slickNext()}
				colorScheme="white"
				_hover={{ bg: "white" }}
				icon={
					<ChevronRightIcon
						color="white"
						w="inherit"
						h="inherit"
						_hover={{ color: "black" }}
					/>
				}
			/>
			{/* Slider */}
			<Slider {...settings} ref={(slider: any) => setSlider(slider)}>
				{cards.map((card, index) => (
					<Box
						key={index}
						position="relative"
						backgroundRepeat="no-repeat"
						backgroundSize="cover"
						backgroundImage={`url(${card.url})`}
					>
						{/* This is the block you need to change, to customize the caption */}
						<Container
							maxW="3xl"
							height={{ base: "xs", md: "xl" }}
							position="relative"
						>
							<Stack
								p={3}
								spacing={6}
								bg="blackAlpha.500"
								position="absolute"
								transform={{
									base: "translate(25%,-50%)",
									md: "translate(0, -50%)",
								}}
								maxW={{ base: "3xs", md: "xl" }}
								top={{ base: "50%", md: "80%" }}
							>
								<Text
									color="white"
									textTransform="uppercase"
									fontWeight="semibold"
									fontSize={{ base: "xs", md: "2xl" }}
								>
									{card.description}
								</Text>
							</Stack>
						</Container>
					</Box>
				))}
			</Slider>
		</Box>
	);
}
