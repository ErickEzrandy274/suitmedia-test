import React from "react";
import { Box, Flex, Text } from "@chakra-ui/react";
import { FacebookIcon, TwitterIcon } from "../Icons";

const Footer = () => {
	return (
		<Flex
			flexDir="column"
			gap={2}
			p={3}
			bg="gray.700"
			color="white"
			textAlign="center"
			fontWeight="semibold"
		>
			<Text>Copyright © 2016. PT Company</Text>
			<Flex justifyContent="center" gap={3}>
				<FacebookIcon />
				<TwitterIcon />
			</Flex>
		</Flex>
	);
};

export default Footer;
