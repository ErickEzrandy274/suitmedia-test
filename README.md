## Tech That I used
1. NextJS
2. Chakra UI
3. Typescript
5. Formik
6. Yup

## How to run the program locally
1. git clone https://gitlab.com/ErickEzrandy274/suitmedia-test.git
2. git fetch (if your local doesn't have a branch named master)
3. git checkout master
4. yarn install
5. yarn dev

# [Link Production](https://suitmedia-test-erickezrandy.vercel.app/)