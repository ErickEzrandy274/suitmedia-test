import React from "react";
import { Flex } from "@chakra-ui/react";
import { Footer, Navbar } from "@elements";
import { ReactNodeProps } from "./interface";

const Layout: React.FC<ReactNodeProps> = ({ children }) => {
	return (
		<Flex flexDir="column" justifyContent="space-between" minH="100vh">
			<Navbar />
			{children}
			<Footer />
		</Flex>
	);
};

export default Layout;
