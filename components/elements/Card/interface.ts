import { ReactNode } from "react";

export interface ValuesCardProps {
	icon: ReactNode;
	title: string;
	description: string;
	type: "innovative" | "loyalty" | "respect";
	isLast?: boolean;
}
