import React, { useMemo } from "react";
import {
	Button,
	Flex,
	FormControl,
	FormErrorMessage,
	FormLabel,
	Input,
	Textarea,
} from "@chakra-ui/react";
import { useFormik } from "formik";
import { CONTACT_US_INPUT, contactUsInputValidation } from "./constant";

const ContactUsForm = () => {
	const { initialValues, validationSchema } = useMemo(() => {
		return {
			initialValues: CONTACT_US_INPUT,
			validationSchema: contactUsInputValidation,
		};
	}, []);

	const formik = useFormik({
		initialValues,
		validationSchema,
		onSubmit: (values, { resetForm }) => {
			if (values) {
				alert(
					`Name: ${values.name}\nEmail: ${values.email}\nMessage: ${values.message}`
				);
				resetForm();
			}
		},
	});

	return (
		<form onSubmit={formik.handleSubmit}>
			<Flex flexDir="column" w={{ md: "xl" }} gap={5} mx="auto">
				<FormControl isInvalid={formik.touched.name && !!formik.errors.name}>
					<FormLabel>Name</FormLabel>
					<Input
						name="name"
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.name}
					/>

					{formik.touched.name && formik.errors.name && (
						<FormErrorMessage fontWeight="semibold" fontSize="sm" mt={1}>
							{formik.errors.name}
						</FormErrorMessage>
					)}
				</FormControl>

				<FormControl isInvalid={formik.touched.email && !!formik.errors.email}>
					<FormLabel>Email</FormLabel>
					<Input
						type="email"
						name="email"
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.email}
					/>

					{formik.touched.email && formik.errors.email && (
						<FormErrorMessage fontWeight="semibold" fontSize="sm" mt={1}>
							{formik.errors.email}
						</FormErrorMessage>
					)}
				</FormControl>

				<FormControl
					isInvalid={formik.touched.message && !!formik.errors.message}
				>
					<FormLabel>Message</FormLabel>

					<Textarea
						name="message"
						rows={5}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						value={formik.values.message}
					/>

					{formik.touched.message && formik.errors.message && (
						<FormErrorMessage fontWeight="semibold" fontSize="sm" mt={1}>
							{formik.errors.message}
						</FormErrorMessage>
					)}
				</FormControl>

				<Button type="submit" colorScheme="linkedin" mb={10}>
					SUBMIT
				</Button>
			</Flex>
		</form>
	);
};

export default ContactUsForm;
