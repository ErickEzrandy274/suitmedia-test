export interface CustomLinkProps {
	href: string;
	name: string;
}
