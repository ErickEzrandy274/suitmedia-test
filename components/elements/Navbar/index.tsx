import React from "react";
import {
	Box,
	Container,
	Flex,
	Heading,
	IconButton,
	useDisclosure,
} from "@chakra-ui/react";
import { AboutMenu, CustomLink } from "@elements";
import { CloseIcon, HamburgerIcon } from "@chakra-ui/icons";
import { navLink } from "./constant";

const Navbar = () => {
	const { isOpen, onOpen, onClose } = useDisclosure();

	return (
		<Container
			display="flex"
			flexDir={{ base: "column", md: "row" }}
			maxW="5xl"
		>
			<Flex
				justifyContent="space-between"
				alignItems="center"
				w="full"
				py={{ base: 3, md: 0 }}
			>
				<Heading>Company</Heading>

				<Flex
					textTransform="uppercase"
					alignItems="center"
					gap={3}
					display={{ base: "none", md: "flex" }}
				>
					<AboutMenu />
					{navLink.map((item) => {
						return <CustomLink key={item.name} {...item} />;
					})}
				</Flex>

				<IconButton
					size="md"
					icon={isOpen ? <CloseIcon /> : <HamburgerIcon />}
					aria-label="Open Menu"
					display={{ md: "none" }}
					onClick={isOpen ? onClose : onOpen}
				/>
			</Flex>

			{isOpen && (
				<Box display={{ md: "none" }} textTransform="uppercase">
					<Flex flexDir="column" as={"nav"} gap={3}>
						<AboutMenu />
						{navLink.map((item) => {
							return <CustomLink key={item.name} {...item} />;
						})}
					</Flex>
				</Box>
			)}
		</Container>
	);
};

export default Navbar;
