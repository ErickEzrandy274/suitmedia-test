export const cards = [
	{
		description: "we don't have the best but we have the greatest team",
		url: "/bg-about.png",
	},
	{
		description:
			"this is a place where technology & creativity fused into digital chemistry",
		url: "/bg-tech.png",
	},
];


export const settings = {
	dots: true,
	arrows: false,
	fade: true,
	infinite: true,
	autoplay: true,
	speed: 500,
	autoplaySpeed: 5000,
	slidesToShow: 1,
	slidesToScroll: 1,
};