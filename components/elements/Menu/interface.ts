import { Dispatch, SetStateAction } from "react";

export interface AboutMenuProps {
	isFocus: string;
	setIsFocus: Dispatch<SetStateAction<string>>;
}
