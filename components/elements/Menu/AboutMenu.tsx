import React, { useState } from "react";
import {
	Box,
	Menu,
	MenuButton,
	MenuList,
	MenuItem,
	useDisclosure,
	Text,
} from "@chakra-ui/react";

const AboutMenu = () => {
	const { isOpen, onOpen, onClose } = useDisclosure();

	const handleClick = (nav: string) => {
		alert(nav);
	};

	return (
		<Menu isOpen={isOpen} closeOnSelect closeOnBlur onClose={onClose}>
			<MenuButton
				as={Box}
				p={3}
				textTransform="uppercase"
				onMouseEnter={onOpen}
				_hover={{ bg: "gray.200" }}
				bg={isOpen ? "gray.200" : "white"}
			>
				<Text>about</Text>
			</MenuButton>

			<MenuList onMouseLeave={onClose}>
				<MenuItem
					_hover={{ bg: "gray.600", color: "white" }}
					onClick={() => handleClick("history")}
				>
					HISTORY
				</MenuItem>
				<MenuItem
					_hover={{ bg: "gray.600", color: "white" }}
					onClick={() => handleClick("vision mission")}
				>
					VISION MISSION
				</MenuItem>
			</MenuList>
		</Menu>
	);
};

export default AboutMenu;
