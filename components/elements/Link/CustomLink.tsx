import React from "react";
import { Link } from "@chakra-ui/react";
import { CustomLinkProps } from "./interface";

const CustomLink: React.FC<CustomLinkProps> = ({ href, name }) => {
	return (
		<Link href={href} p={3} _hover={{ bg: "gray.200" }}>
			{name}
		</Link>
	);
};

export default CustomLink;
