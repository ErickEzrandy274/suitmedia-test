import { BalancedscaleIcon, BankIcon, LightbulbIcon } from "@elements";
import { ValuesCardProps } from "components/elements/Card/interface";

export const ourValuesData: ValuesCardProps[] = [
	{
		icon: <LightbulbIcon />,
		title: "innovative",
		description:
			"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi quis, inventore iure aperiam rem optio magnam quibusdam perferendis sit ipsa",
		type: "innovative",
	},
	{
		icon: <BankIcon />,
		title: "loyalty",
		description:
			"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi quis, inventore iure aperiam rem optio magnam quibusdam perferendis sit ipsam voluptate maxime iusto unde, nemo neque?",
		type: "loyalty",
	},
	{
		icon: <BalancedscaleIcon />,
		title: "respect",
		description:
			"Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi quis, magnam quibusdam perferendis sit ipsam voluptate maxime iusto unde, nemo neque? Incidunt at vitae doloribus.",
		type: "respect",
	},
];
