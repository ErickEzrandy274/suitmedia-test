import { object, string } from "yup";

export type ContactUsType = {
	name: string;
	email: string;
	message: string;
};

export const CONTACT_US_INPUT: ContactUsType = {
	name: "",
	email: "",
	message: "",
};

export const alphabetOnlyRegex = /^[A-Za-z ]+$/;

export const contactUsInputValidation = object({
	name: string()
		.required("This field is required!")
		.matches(alphabetOnlyRegex, "Your name is not valid!"),
	email: string()
		.email("Invalid email address!")
		.required("This field is required!"),
	message: string().required("This field is required!"),
});
