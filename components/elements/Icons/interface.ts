export interface IconProps {
	width?: number;
	height?: number;
	onClick?: () => void;
}
